Membership Goals
===
ECC is a group formed with the intention of creating something greater than the sum of its parts. As such, membership is not a given; It comes with some expectation of participation and collaboration. To that end, we have multiple levels of membership and a process for joining these levels that makes it clear what's expected of you and what you can expect from the group. Hopefully, this will allow everyone who is interested to participate to a degree they feel comfortable and happy with.

Membership Types
===

Edge Case
---
Edge Cases are active participants in everything we do as a group. They are expected to, to some degree, manage programming on the channel, participate actively in major events, and take part in making major decisions about the direction of the team. They are also expected to lead committees on projects and events.

They are also expected to represent the group well, and create what opportunities they can, both for their teammates and for the queer gaming community as a whole, through hosting or collaborating in their own spaces, so that we can all grow together.

Edge Cases will also meet on discord at least once a month in order to bring everyone up to date on new business and plan new projects or programming. After Edge Case specific discussion is done, this meeting will be opened up to Exceptions and Unit Tests both as a social space and for any further discussion that involves them.

Exception
---
Exceptions are expected to participate in events much as Edge Cases are, but there is no particular expectation around the consistency of their participation. They may but are not required to lead projects or programming. In general, this level of membership exists to provide a training ground for becoming an Edge Case, but there is no requirement that anyone with this level of membership will definitely become an Edge Case.

As with Edge Cases, there is an expectation of representing the group in a positive light.

Exceptions are able to participate in the second half of the monthly meeting, but are not required to do so.

Debugger
---
Debuggers have volunteered their time or effort to one or more specific projects, shows, or tasks that the group has taken on. This could include, for example, moderating the twitch channel or restreaming for events.

Unit Tests
---
Unit Tests are people who Edge Cases believe have insight into organizational issues. They may be members of other groups that we want to collaborate with, experts in certain useful fields, or just generally people who are able to contribute to important discussions but have no particular vested interest in the group. They may also act as mediators in the event of a high level dispute among Edge Cases.

Membership Policies and Processes
===

Elections and Voting Structure
---
Decision making for the structure and nature of the team will be done largely through some variant of a consensus voting system. For more information on what that means, see https://en.wikipedia.org/wiki/Consensus_decision-making. For our purposes though, this means that for major decisions (membership and constitutional issues in particular), Edge Cases are capable of vetoing decisions until their concerns are met. This should obviously be used carefully, and is also a reason why we have to be careful about who holds decision making power in the group.

All votes for major things will have 4 possible votes:

- **Affirmative**: This is an unqualified positive vote.
- **Negative**: A negative vote that does not, in itself, prevent the proposal from going forward.
- **Reservations**: A negative vote that is open for discussion, if some condition is met (like changing the proposal or with caveats the potential member must agree to before acceding to their membership). If those reservations are not dealt with, this vote blocks the proposal. It is an extremely constructive way of dealing with any issues with a proposal and should be used generously when there are real concerns.
- **Veto**: An unchangeable and unqualified negative vote. If allowed by the rules of the particular vote/election, it blocks the proposal. This should be used extremely sparingly and only with grave reservations. It is an important tool, but not one to be used lightly.

Unless otherwise specified, votes are simple majority votes where more than 50% of voters have to have cast affirmative votes, with no vetos or unmet reservations. Specific votes may require a higher threshold (for eg. 75%) if they are of extreme importance, or the ability to veto unconditionally may not be allowed (if the vote is not existential).

Whenever a discussion of negative aspects of a person are to be had (for eg. because of reservations about a potential Exception member or for all Edge Case votes), these must take place in an Edge Cases only channel that will be deleted after the discussion is complete, with an agreed on summary of the general sentiment placed in the permanently recorded notes for the meeting. This is to avoid colouring their experience should they become a member while allowing free discussion of the proposal.

Process
---
Becoming a member of ECC will be an open process. In order to maintain stability and to make sure that members can get to know each other fairly well, growth of the Edge Case and Exception levels of the team will be rate limited, and growing your involvement with the team will take time and effort.

Anyone in good standing can volunteer for an event or project being run by an ECC member or the team as a whole. This is generally the first step to becoming an Edge Case and doing so is the absolute best way to signal your interest in being a part of the team. Being a volunteer on a project or event is the most basic level of membership and is effectively automatic and uncapped.

Periodically, Edge Cases will get together to decide on admitting people to either Exception or Edge Case membership. Debuggers in particular are automatically eligible to become Exceptions, but anyone well known to the existing Edge Cases is also eligible if they bring specific skills or knowledge to the team. It is ok to ask members to be considered for this, even if you have not specifically volunteered in the past. Nomination by this path is by discretion of the Edge Cases doing the vouching. Eligibility for becoming an Edge Case comes only with some period of being an Exception and demonstrating a strong interest in deepening involvement.

In deciding who is considered for moving up, members that have expressed interest or are believed to be interested in doing so, and are eligible, will be nominated and seconded by Edge Cases. Anyone so nominated will be discussed and voted on in the specific process described below.

To move up from Debugger to Exception, a vote will be held in the membership meeting channel on discord after a short discussion. This discussion should focus on the positives the person will bring to the group. This vote will only require a simple majority of positive votes, but any member may ask to move the discussion to a private space if they have reservations, and their reservations must be dealt with before proceeding on that membership. This private space must be deleted after the issue is dealt with.

For becoming an Edge Case the bar will be fairly high, and growth of Edge Case membership will be capped at 2 per half calendar year (jan-jun, jul-dec). If there are more than two potential Edge Cases, the existing Edge Cases can choose whatever method is appropriate to the quantity and situation to rank them in order of consideration. Then each one will have a channel created to hold an in depth discussion of their suitability for Edge Case membership in order, and a vote will be held within a day of this discussion beginning, until the Edge Case cap has been reached.

Membership 'slots' will not carry over from one period to the next and only a unanimously positive vote will be able to raise the cap temporarily. Once this document is approved, it can't be raised permanently.

Removal
---

Should a member of any level violate the community's best interests in some way (harrassment, abuse, misappropriation of group resources, unwanted sexual advances, and other similarly egregiously negative behaviour), a petition in good faith by people directly affected by the offending behaviour will trigger a process of removal. 

This is the same process as for the addition of an Edge Case member, regardless of their level of involvement, and requires the same threshold of agreement for them to continue to have good standing with the group. If their good standing is rejected, any and all membership rights and responsibilities will be removed for at least 6 months, and at the discression of the Edge Cases this may be extended to any longer amount of time (including indefinitely).

If an Edge Case is under review in this manner, they may not vote on their own membership. If an Edge Case is one of the petitioners, they also may not vote. If both parties are Edge Cases then the situation is beyond the provisions of this document to resolve and the group of Edge Cases as a whole must determine a path forward for the group.

Stepping Down
---

It's expected that people may need to step back from their responsibilities from time to time. This should be discussed with the Edge Cases, and outcomes will be on a case by case basis for now until and unless experience dictates a formal process be established. If someone needs to indefinitely step back from their role, they may need to go back through the process that brought them to the level of membership they originally attained. Debugger status is permanent so long as good standing is maintained, however.

Qualifications
---

Although this is a queer-focused group, identification with that label is not an explicit requirement for any level of membership. However, if you are not yourself queer you should have strong evidence of both being an ally to queer causes and a sense for the strange things in video games.

However, when considering new Debuggers, Exceptions, and Edge Cases, an effort should be made to recognize intersectionality and to make a strong effort to favour growing our membership of minority and marginalized groups, either in society as a whole or specifically within gaming. Queer people and especially QTBIPOC (Queer and Trans Black Indigenous and People of Colour) belong in our spaces and we should make room for them and elevate their voices as best we can.

In general participation in our events is not by default limited to members. You do not have to be a member of any sort in order to be on a restreamed event with one of our members, to work on or test a project one of our member is working on, or to participate in one of our marathons. Our goal is definitely to create a space where queer people in particular can comfortably enjoy themselves without taking on additional burdens in their lives.


Current Membership
===

Edge Cases
---
| Name | Pronouns | Time Zone | Twitch URL | Twitter Handle |
| ---- | -------- | --------- | ---------- | -------------- |
| Amethyst_Rocks | she/her | Eastern | https://twitch.tv/amethyst_rocks | https://twitter.com/ame_rocks |
| GarbiTheGlitcheress | she/her | Central | https://twitch.tv/garbitheglitcheress | https://twitter.com/GarbiGlitchress |
| leggystarscream | she/her | Pacific | https://twitch.tv/leggystarscream | https://twitter.com/leggystarscream |
| megMacAttack | she/her or they/them | Mountain | https://twitch.tv/megmacattack | https://twitter.com/_megmac_ |
| mustbetuesdaymusic | they/them | Mountain | https://twitch.tv/mustbetuesdaymusic | https://twitter.com/mustbetuesday |
| rubash319 | she/her or they/them | Central | https://twitch.tv/rubash319 | https://twitter.com/rubash319 |
| seeliefae | fae/faer | Eastern | https://twitch.tv/seeliefae | https://twitter.com/seeliefaetv |
| ZoeVermilion | she/her or xe/xem | Central | https://twitch.tv/zoevermilion | https://twitter.com/zoevermilion |

Exceptions
---
| Name | Pronouns | Time Zone | Twitch URL | Twitter Handle |
| ---- | -------- | --------- | ---------- | -------------- |
| dijonketchup | she/her | Central | https://twitch.tv/dijonketchup | https://twitter.com/dijonketchup13 |
