Edge cases are the special, specific places in algorithms where the math or logic breaks down in general, and specific alternative rules need to come into play. The most well known edge case is likely the one taught in elementary school math class - dividing by zero. We start by learning that we can't divide by zero, but we can divide by any other number - and this immutable fact, this law of nature holds until we reach calculus, and learn the why... But also the particular circumstances where we can.
 
Of course, computers are simple creatures, and have to handle these edge cases in a few ways - throwing an error (the digital equivalent of saying "This isn't allowed here") is a common one. But, with care, patience and a little bit of foresight, we can build systems that look out for these different edge cases, and build ways of handling them appropriately, because they cannot be properly treated by the default.
 
The ways we exist as people, as societies, follow a similar path - we're taught there are boys and girls, and they get married to exactly one of the other - and maybe this goes on, but eventually we grow. We learn. We discover about ourselves, and maybe we aren't a boy or a girl. Maybe we don't get married, or maybe we do, but not to the right sort of person.
 
And maybe we exist in a state that was never expected, was never handled by the societal algorithm we all move through. In many cases, existing outside of these scripts throws societal exceptions, because the systems weren't designed, purposefully or otherwise, for the sorts of people we are.
 
But, the digital has a benefit, above all else: it allows the outcasts, the marginalized, to find edge other, to band together over shared interests and experiences; of finding the breaks in algorithms and society.
 
We are the people the systems never expected playing with systems in the ways that people never expected
 
We are the uncaught exceptions to the rules.
 
We are the Edge Case Collective.